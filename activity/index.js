
const firstNameInput = document.getElementById('firstName');

const lastNameInput = document.getElementById('lastName');

const outputSpan = document.getElementById('output');


function updateOutput() {
  outputSpan.textContent = firstNameInput.value + ' ' + lastNameInput.value;
}


lastNameInput.addEventListener('input', updateOutput);
